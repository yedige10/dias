<?php

namespace App\Http\Controllers;
use App\Mail\SendEmailDoctor;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use DB;

class RouteController extends Controller
{
    public function index()

    {
        return view('welcome');
    }
    public function send(Request $request)
    {   
        $name=$request->content['name'];
        $phone=$request->content['phone'];
        DB::insert('insert into example ( name,phone) values ( ?,?)', [ $name,$phone]);
        
        return response()->json([
            'success'=>true
        ]);
    }
    public function getting()
    {   
       
        $posts=DB::table('example')->get();
        
        return response()->json([
            'posts'=>$posts
        ]);
    }

}
