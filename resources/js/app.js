/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import router from '../js/router/route'
window.Vue = require('vue');
import App from '../js/components/App'
import VModal from 'vue-js-modal'
import VueResource from 'vue-resource'
import VueInstagram from 'vue-instagram'
Vue.use(VModal, { dynamic: true, injectModalsContainer: true })
Vue.use(VueResource)
Vue.use(VueInstagram)
Vue.http.headers.common['X-CSRF-TOKEN'] =  $('meta[name="csrf-token"]').attr('content')
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
