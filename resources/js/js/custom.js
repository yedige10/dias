$(document).ready(function() {


    var userFeed = new Instafeed({
        get: 'user',
        userId: '6213914581',
        limit: 12,
        resolution: 'standard_resolution',
        accessToken: '6213914581.1677ed0.f21408abbca14c4996b27c31aae88968',
        sortBy: 'most-recent',
        template: '<div class="col-lg-3 instaimg"><a href="{{image}}" title="{{caption}}" target="_blank"><img src="{{image}}" alt="{{caption}}" class="img-fluid"/></a></div>',
    });


    userFeed.run();

    
    // This will create a single gallery from all elements that have class "gallery-item"
    $('.gallery').magnificPopup({
        type: 'image',
        delegate: 'a',
        gallery: {
            enabled: true
        }
    });


});